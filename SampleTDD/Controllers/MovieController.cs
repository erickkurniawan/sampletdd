﻿using SampleTDD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleTDD.Controllers
{
    public class MovieController : Controller
    {
        private IMovieRepository _repository;
        public MovieController(IMovieRepository repository)
        {
            _repository = repository;
        }

        // GET: Movie
        public ActionResult Index()
        {
            var model = _repository.GetAll();
            return View(model);
        }

        public ActionResult FindMovieById(int movieId)
        {
            var model = _repository.FindById(movieId);
            return View(model);
        }
    }
}