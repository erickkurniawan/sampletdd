namespace SampleTDD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Field_Description : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Movies", "Description");
        }
    }
}
