﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleTDD.Models
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> GetAll();
        Movie FindById(int movieId);

    }

    public class MovieRepository : IMovieRepository
    {
        private MovieDataContext dataContext;
        public MovieRepository()
        {
            dataContext = new MovieDataContext();
        }

        public Movie FindById(int movieId)
        {
            var result = (from m in dataContext.Movies
                          where m.MovieID == movieId
                          select m).SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            else
            {
                throw new Exception("Data tidak ditemukan");
            }
        }

        public IEnumerable<Movie> GetAll()
        {
            return dataContext.Movies.OrderBy(m => m.Title);
        }
    }
}