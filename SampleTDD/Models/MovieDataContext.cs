﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SampleTDD.Models
{
    public class MovieDataContext : DbContext
    {
        public MovieDataContext() : base("MyConnection")
        {}

        public DbSet<Movie> Movies { get; set; }
    }
}