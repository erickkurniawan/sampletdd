﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleTDD.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public int CategoryID { get; set; }
        public string Title { get; set; }
        public string Actor { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
    }
}