﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleTDD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleTDDIntegration
{
    [TestClass]
    public class SampleIntegrationTest
    {
        [TestMethod]
        public void Cek_GetAllMovie()
        {
            MovieRepository repo = new MovieRepository();
            int expected = 2;
            var movies = repo.GetAll();

            Assert.AreEqual(expected, movies.Count());
        }
    }
}
