﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleTDD.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SampleTDD.Tests.Controllers
{
    [TestClass]
    public class FizzBuzzControllerTest
    {
        [TestMethod]
        public void Given1Return1()
        {
            FizzBuzzController controller = new FizzBuzzController();
            ViewResult result = controller.Index(1) as ViewResult;
            string expected = "1";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Given3Return12Fizz()
        {
            FizzBuzzController controller = new FizzBuzzController();
            ViewResult result = controller.Index(3) as ViewResult;
            string expected = "12Fizz";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Given5Return12Fizz4Buzz()
        {
            FizzBuzzController controller = new FizzBuzzController();
            ViewResult result = controller.Index(5) as ViewResult;
            string expected = "12Fizz4Buzz";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void Given6Return12Fizz4BuzzFizz()
        {
            FizzBuzzController controller = new FizzBuzzController();
            ViewResult result = controller.Index(6) as ViewResult;
            string expected = "12Fizz4BuzzFizz";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Given15Return12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz()
        {
            FizzBuzzController controller = new FizzBuzzController();
            ViewResult result = controller.Index(15) as ViewResult;
            string expected = "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz";
            string actual = result.ViewBag.Output;
            Assert.AreEqual(expected, actual);
        }
    }
}
