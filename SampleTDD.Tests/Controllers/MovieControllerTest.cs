﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SampleTDD.Controllers;
using SampleTDD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SampleTDD.Tests.Controllers
{
    [TestClass]
    public class MovieControllerTest
    {
        private List<Movie> movies = new List<Movie>()
            {
                new Movie {MovieID=1, Title="Dilan 1990"},
                new Movie {MovieID=2, Title="Thor"}
            };

        [TestMethod]
        public void it_should_render_the_default_view()
        {
            var repository = new Mock<IMovieRepository>();
            repository.Setup(r => r.GetAll()).Returns(movies);

            var controller = new MovieController(repository.Object);
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void it_should_pass_movies_as_the_model()
        {
            var repository = new Mock<IMovieRepository>();
            repository.Setup(r => r.GetAll()).Returns(movies);

            var controller = new MovieController(repository.Object);

            var result = controller.Index() as ViewResult;

            var model = result.ViewData.Model as List<Movie>;

            Assert.IsTrue(movies.Equals(model));
        }

        [TestMethod]
        public void return_tipe_movie()
        {
            var repo = new Mock<IMovieRepository>();
            repo.Setup(m => m.FindById(It.IsAny<int>())).Returns((int i) => movies.Where(x => x.MovieID == i).Single());

            var resultMovie = repo.Object.FindById(2);

            var controller = new MovieController(repo.Object);
            var result = controller.FindMovieById(2) as ViewResult;
            var model = result.ViewData.Model as Movie;

            Assert.IsNotNull(resultMovie);
            Assert.IsInstanceOfType(resultMovie, typeof(Movie));
            //Assert.AreEqual("Thor", resultMovie.Title);
            Assert.AreEqual(resultMovie,model);
        }
    }
}
